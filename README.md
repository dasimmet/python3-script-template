# Python Script Template

this is a complete gitlab ci for python projects with:

- pep8 validataion
- parralel tests on multiple python versions
- Junit xml compatible test reports
- sphinx docs build with markdown enabled
- Html Coverage built into pages
- a manual job for pip packge registry publishing
- a basic argument parser documented by sphinx-argparse

## Fork Command

```bash
python -m python_script_template fork "<projectname>" --dryrun
```

## Clean Code with autopep8

```bash
python -m autopep8 -ir python_script_template
```