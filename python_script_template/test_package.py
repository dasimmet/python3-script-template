#!/usr/bin/env python3

from unittest import TestCase


class PackageTest(TestCase):
    def test_import(self):
        import pkgutil
        import os
        import importlib.util
        import sys
        dirname = os.path.dirname(__file__)
        for importer, package_name, _ in pkgutil.iter_modules([dirname]):
            if package_name == '__main__':
                continue
            full_package_name = '%s.%s' % (dirname, package_name)
            print(full_package_name)
            spec = importer.find_spec(package_name)
            module = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(module)
            print(module)

    def test_run_main(self):
        from .cli import main
        self.assertRaises(SystemExit, main, argv=["--help"])

    def test_fork(self):
        from .cli import main
        self.assertEqual(0, main(['fork', 'test_fork']))
