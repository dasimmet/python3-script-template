#!/usr/bin/env python3

from argparse import Namespace, ArgumentParser


def main(argv=None):
    '''This is the main function of the package's cli
    '''
    import sys
    argv = sys.argv[1:] if argv == None else argv
    p = parser()
    args = p.parse_args(argv)
    return args.func(args) or 0


def parser():
    '''returns an argument parser
    '''
    p = ArgumentParser(description="an empty argument parser as an example")
    p.set_defaults(func=lambda x: p.print_help())
    # p.add_argument('-t','--testint', type=int, help="an integer hello world will print")
    sub = p.add_subparsers()
    p_fork = sub.add_parser('fork')
    p_fork.set_defaults(func=fork)
    p_fork.add_argument('project_name', type=str, help="project name")
    p_fork.add_argument('--dryrun', action='store_true')
    return p


def fork(args: Namespace):
    name = args.project_name
    import re
    import os
    import subprocess
    rx = re.compile('python([-_])script([-_])template')
    if not __file__.startswith(os.getcwd()):
        raise Exception('Script {} Called from an external directory: {}'.format(__file__, os.getcwd()))
    tree = subprocess.check_output(['git', 'ls-tree', '--full-tree', '--name-only', '-r', 'HEAD']).decode().splitlines()
    for f in tree:
        dir, fname = os.path.split(f)
        match_dir = rx.sub(name, dir)
        match_f = rx.sub(name, fname)
        if match_dir != dir:
            new_f = os.path.join(match_dir, fname)
            print('NEWDIR:', match_dir, dir)
            dir = match_dir
            if not args.dryrun:
                os.makedirs(match_dir, exist_ok=True)
                subprocess.check_call(['git', 'mv', f, new_f])
                f = new_f
        if match_f != fname and not args.dryrun:
            new_f = os.path.join(dir, f)
            subprocess.check_call(['git', 'mv', f, new_f])
            f = new_f

        with open(f) as fd:
            content = fd.read()

        new_content = rx.sub(name, content)
        if new_content != content:
            print('Adapt Content:', f)
            if not args.dryrun:
                with open(f, 'w') as fd:
                    fd.write(new_content)
                subprocess.check_call(['git', 'add', f])
    return 0


def hello_world(args: Namespace):
    '''Prints Hello World to the command line
    '''
    print("Hello World: testint=" + str(args.testint))
