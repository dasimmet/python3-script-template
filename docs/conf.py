# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

import datetime
from os import environ

project = environ.get("CI_PROJECT_NAME", 'gitlab_script_template')
copyright = '{}, Tobias Simetsreiter'.format(datetime.datetime.now().year)
author = 'Tobias Simetsreiter'


# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'myst_parser',
    'sphinx.ext.todo',
    'sphinx.ext.viewcode',
    'sphinx.ext.autodoc',
    'sphinxarg.ext',
]

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']



# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "sphinx_rtd_theme"
html_static_path = ['_static']
source_suffix = {
    '.rst': 'restructuredtext',
    '.txt': 'markdown',
    '.md': 'markdown',
}

myst_url_schemes = {
    "http": None,
	"https": None,
    "relative": "{{netloc}}{{path}}",
    "pages": environ.get("CI_PAGES_URL", "https://dasimmet.gitlab.io/python_script_template") +  "/{{netloc}}{{path}}#{{fragment}}",
    "repo": environ.get("CI_PROJECT_URL", "https://gitlab.com/dasimmet/python_script_template") +  "/{{netloc}}{{path}}#{{fragment}}",
}
